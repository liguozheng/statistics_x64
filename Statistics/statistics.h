#ifndef STATISTICS_H
#define STATISTICS_H

#include <QtWidgets/QMainWindow>
#include "ui_statistics.h"
#include "QPrintThread.h"
#include <QProgressDialog>
#include "QGlobal.h"
class Statistics : public QMainWindow
{
	Q_OBJECT

public:
	Statistics(QWidget *parent = 0);
	~Statistics();
private slots:
    void on_pushButton_clicked();
	void processStart(const QString&);
    void processEnd(const QString&);
private:
	Ui::StatisticsClass ui;
	QGlobal* m_global;
	QPrintThread* m_print;
	QProgressDialog* m_progressDialog;
};

#endif // STATISTICS_H
