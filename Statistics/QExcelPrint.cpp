#include "QExcelPrint.h"
#include <ObjBase.h>

QExcelPrint::QExcelPrint(QObject *parent)
: QObject(parent)
{
	m_filepath = QString("%1/bin/print.xlsx").arg(g_path);
	m_workbook = new QAxObject();
	m_excel = new QAxObject();
}

QExcelPrint::~QExcelPrint()
{

	if (!m_workbook->isNull()){
		m_workbook->dynamicCall("Close()");//关闭工作簿
	}
	if (!m_excel->isNull()){
		m_excel->dynamicCall("Quit()");//关闭excel
		delete m_excel;
		m_excel = NULL;
	}
}

bool QExcelPrint::exportToExcel()
{
	   CoInitializeEx(NULL, COINIT_MULTITHREADED);
		m_excel = new QAxObject(this);
		m_excel->setControl("Excel.Application");//连接Excel控件
		if (!m_excel){
			return false;
		}
		m_excel->setProperty("DisplayAlerts", false);//不显示任何警告信息。如果为true那么在关闭是会出现类似“文件已修改，是否保存”的提示
		QAxObject *workbooks = m_excel->querySubObject("WorkBooks");//获取工作簿集合
		if (!workbooks){
			return false;
//		workbooks->dynamicCall("Add");//新建一个工作簿
		}
		m_workbook = workbooks->querySubObject("Open(const QString&)", m_filepath);
		if (!m_workbook){
			return false;
		}
//		workbook = excel->querySubObject("ActiveWorkBook");//获取当前工作簿
		QAxObject *worksheets = m_workbook->querySubObject("Sheets");//获取工作表集合
		if (!worksheets){
			return false;
		}
		m_worksheet = worksheets->querySubObject("Item(int)", 1);//获取工作表集合的工作表1，即sheet1
		if (!m_worksheet){
			return false;
		}
		
		return true;
}

void QExcelPrint::setCellValue(QString row, int column, const QString &value)
{
	QString Qstr = row + QString::number(column);//设置要操作的单元格，如A1
	QAxObject *cell = m_worksheet->querySubObject("Range(QVariant, QVariant)", Qstr);//获取单元格
	cell->dynamicCall("SetValue(const QVariant&)", QVariant(value));//设置单元格的值
}

bool QExcelPrint::printview(QString ValPrintFile)
{
	
		CoInitializeEx(NULL, COINIT_MULTITHREADED);
		m_excel = new QAxObject(this);
		m_excel->setControl("Excel.Application");//连接Excel控件
		if (!m_excel){
			return false;
		}
		m_excel->setProperty("DisplayAlerts", false);//不显示任何警告信息。如果为true那么在关闭是会出现类似“文件已修改，是否保存”的提示
		QAxObject *workbooks = m_excel->querySubObject("WorkBooks");//获取工作簿集合
		if (!workbooks){
			return false;
			//		workbooks->dynamicCall("Add");//新建一个工作簿
		}
		m_workbook = workbooks->querySubObject("Open(const QString&)", ValPrintFile);
		QAxObject *worksheets = m_workbook->querySubObject("Sheets");//获取工作表集合
		if (!worksheets){
			return false;
		}
		m_worksheet = worksheets->querySubObject("Item(int)", 1);//获取工作表集合的工作表1，即sheet1
		if (!m_worksheet){
			return false;
		}
		m_worksheet->querySubObject("PrintPreview");
		return true;
}
