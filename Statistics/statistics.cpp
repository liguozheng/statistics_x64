#include "statistics.h"
#pragma execution_character_set("utf-8")
Statistics::Statistics(QWidget *parent)
	: QMainWindow(parent)
{
	ui.setupUi(this);
	m_global = new QGlobal(this);
	m_print = new QPrintThread();
	ui.dateEdit_start->setDisplayFormat("yyyy-MM-dd");
	ui.dateEdit_end->setDisplayFormat("yyyy-MM-dd");
	ui.dateEdit_start->setDateTime(QDateTime::currentDateTime());
	ui.dateEdit_end->setDateTime(QDateTime::currentDateTime());
	ui.dateEdit_start->setCalendarPopup(true);
	ui.dateEdit_end->setCalendarPopup(true);
	connect(m_print, SIGNAL(processStart(const QString&)), this, SLOT(processStart(const QString&)));
	connect(m_print, SIGNAL(processEnd(const QString&)), this, SLOT(processEnd(const QString&)));
}

Statistics::~Statistics()
{
	delete m_global;
	delete m_print;
}

void Statistics::on_pushButton_clicked()
{
	QString SerialStart = ui.dateEdit_start->text() + " 00:00:00";
	QString Serialstop = ui.dateEdit_end->text() + " 23:59:59";
	m_print->setVal(SerialStart, Serialstop);
	m_print->start();
}

void Statistics::processStart(const QString&)
{
	m_progressDialog = new QProgressDialog(this);
	m_progressDialog->setRange(0, 0);
	m_progressDialog->setWindowTitle(QStringLiteral("Deal"));  //设置进度对话框标题
	m_progressDialog->setLabelText(QStringLiteral("Dealing..."));//设置进度对话框显示文本
	m_progressDialog->exec();
}

void Statistics::processEnd(const QString&)
{
	m_progressDialog->close();//关闭进度对话框
}
