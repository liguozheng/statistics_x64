#pragma once
#include <QThread>
#include <QObject>
#include "QGlobal.h"
#include "QExcelPrint.h"
#include <QDateTime>
#include <QSqlQuery>
#include "QGlobal.h"
typedef struct GZ_INFO_TAG
{
	QString CarInfo;     //车号
	QString PLCInputNum; //进纸光电数
	QString PLCOutputNum;//出纸光电数
	QString NodeNum;     //识别号码数
}GZ_INFO;
typedef struct PRINT_INFO_TAG
{
	QString CarInfo; //车号
	QString PLCInputNum; //进纸光电数
	QString PLCOutputNum;//出纸光电数
	QString NodeNum;     //识别号码数
	QString CFNum;       //重复数
	QString JSNoModifyNum;//拒识未修改
	QString JSModifyNum;  //拒识已修改
	QString BPNum;       //白票
	QString YCNum;       //异常数据
	QString CYNum;       //抽样总数
}PRINT_INFO;
class QPrintThread : public QThread
{
	Q_OBJECT
signals :
	void processStart(const QString &);   //处理开始的信号 
	void processEnd(const QString &);     //处理结束的信号
public:
	QPrintThread();
	~QPrintThread();
	void run();
	bool initsql();
	void setVal(QString startTime,QString endTime);
private:
	bool getCarNum(std::vector<GZ_INFO>&  gz_info);
	QString statRepeatCount(QString carInfo);
	QString statRejectNoModifyCount(QString carInfo);
	QString statRejectModifyCount(QString carInfo);
	QString statWhiteCount(QString carInfo);
	QString statAbnormalCount(QString carInfo);
	QString statSampleCount(QString carInfo);
	QSqlDatabase m_db;
	QSqlQuery *m_query;
	QExcelPrint* m_print;
	QString m_startTime;
	QString m_endTime;
};
