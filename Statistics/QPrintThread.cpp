#include "QPrintThread.h"


QPrintThread::QPrintThread()
{
	initsql();
	m_query= new QSqlQuery(m_db);
	m_print = new QExcelPrint(this);
}

QPrintThread::~QPrintThread()
{
	delete m_query;
	delete m_print;
}
bool QPrintThread::initsql()
{
	m_db = QSqlDatabase::addDatabase("QODBC");
	m_db.setDatabaseName(QString("DRIVER={SQL SERVER};"
		"SERVER=%1;"
		"DATABASE=%2;"
		"UID=%3;"
		"PWD=%4;").arg("(local)")
		.arg("penma")
		.arg("sa")
		.arg("123"));
	if (!m_db.open())
	{

		return false;
	}

	return true;
}

void QPrintThread::setVal(QString startTime, QString endTime)
{
	m_startTime = startTime;
	m_endTime = endTime;
}

bool QPrintThread::getCarNum(std::vector<GZ_INFO>&  gz_info)
{
	bool ret = false;
	QString sql = QString("SELECT 车号,PLC入产品数,PLC出产品数,识码出产品数 FROM gz where 生产日期>'%1' and 生产日期<'%2'").arg(m_startTime).arg(m_endTime);
	QSqlQuery query(sql, m_db);
	while (query.next())
	{
		GZ_INFO gz;
		gz.CarInfo = query.value(0).toString();
		gz.PLCInputNum = query.value(1).toString();
		gz.PLCOutputNum = query.value(2).toString();
		gz.NodeNum = query.value(3).toString();
		gz_info.push_back(gz);
		ret &= true;
	}
	return ret;
}

QString QPrintThread::statRepeatCount(QString carInfo)
{
	QString count;
	QString sql = QString("SELECT 原号,COUNT(1) AS Num , COUNT(1) -( SELECT SUM(标识 / 20 * 20) / 20  FROM [%1] WHERE 原号 = a.原号) AS 未确认数 FROM [%1] a WHERE (标识 <> 1 and 标识 <> 7  and 标识<100) GROUP BY 原号 HAVING (COUNT(1) > 1)").arg(carInfo).arg(carInfo);
	QSqlQuery query(m_db.database());
	query.exec(sql);
	query.last();
	int record = query.at() + 1;
	count = QString("%1").arg(record);
	return count;
}

QString QPrintThread::statRejectNoModifyCount(QString carInfo)
{
	QString count;
	QString sql = QString("Select count(编号) as Num from  [%1] where 标识=1").arg(carInfo);
	QSqlQuery query(sql, m_db);
	while (query.next())
	{
		count = query.value(0).toString();
	}
	return count;
}
QString QPrintThread::statRejectModifyCount(QString carInfo)
{
	QString count;
	QString sql = QString("Select count(编号) as Num from  [%1] where 标识=5").arg(carInfo);
	QSqlQuery query(sql, m_db);
	while (query.next())
	{
		count = query.value(0).toString();
	}
	return count;
}

QString QPrintThread::statWhiteCount(QString carInfo)
{
	QString count;
	QString sql = QString("Select count(编号) as Num from  [%1] where (标识=101 OR 标识=105) ").arg(carInfo);
	QSqlQuery query(sql, m_db);
	while (query.next())
	{
		count = query.value(0).toString();
	}
	return count;
}

QString QPrintThread::statAbnormalCount(QString carInfo)
{
	QString count;
	QString sql = QString("Select count(编号) as Num from  [%1] where 标识>=200 and 标识<900 ").arg(carInfo);
	QSqlQuery query(sql, m_db);
	while (query.next())
	{
		count = query.value(0).toString();
	}
	return count;
}
QString QPrintThread::statSampleCount(QString carInfo)
{
	QString count;
	QString sql = QString("Select count(编号) as Num from  [%1] where 抽样标识>0 ").arg(carInfo);
	QSqlQuery query(sql, m_db);
	while (query.next())
	{
		count = query.value(0).toString();
	}
	return count;
}
void QPrintThread::run()
{
	emit processStart(QStringLiteral("开始"));
	m_print->exportToExcel();
	QDateTime current_date_time = QDateTime::currentDateTime();
//	QString current_time = current_date_time.toString("yyyy/MM/dd hh:mm:ss"); //打印日期
	QString currentPath = current_date_time.toString("yyyy-MM-dd-hh-mm-ss"); //打印日期
	int rowNum = 2;//从excel第二行开始写入数据
	std::vector<GZ_INFO> gz_info;
	memset(&gz_info, 0, sizeof(GZ_INFO));
	getCarNum(gz_info);
	for (int i = 0; i < gz_info.size(); i++)
	{
		PRINT_INFO print_info;
		QString carInfo;
		carInfo= gz_info[i].CarInfo;
		print_info.CarInfo = gz_info[i].CarInfo;
		print_info.PLCInputNum = gz_info[i].PLCInputNum;
		print_info.PLCOutputNum = gz_info[i].PLCOutputNum;
		print_info.NodeNum = gz_info[i].NodeNum;
		print_info.CFNum = statRepeatCount(carInfo);  
		print_info.JSNoModifyNum=statRejectNoModifyCount(carInfo);
		print_info.JSModifyNum=statRejectModifyCount(carInfo);
		print_info.BPNum=statWhiteCount(carInfo);
		print_info.YCNum= statAbnormalCount(carInfo);
		print_info.CYNum= statSampleCount(carInfo);

		m_print->setCellValue("A", rowNum, print_info.CarInfo);
		m_print->setCellValue("B", rowNum, print_info.PLCInputNum);
		m_print->setCellValue("C", rowNum, print_info.PLCOutputNum);
		m_print->setCellValue("D", rowNum, print_info.NodeNum);
		m_print->setCellValue("E", rowNum, print_info.CFNum);
		m_print->setCellValue("F", rowNum, print_info.JSNoModifyNum);
		m_print->setCellValue("G", rowNum, print_info.JSModifyNum);
		m_print->setCellValue("H", rowNum, print_info.BPNum);
		m_print->setCellValue("I", rowNum, print_info.YCNum);
		m_print->setCellValue("J", rowNum, print_info.CYNum);
		rowNum++;
	}
	m_print->m_excel->setProperty("Visible", true);
	QString filepath =QString("%1/StaData/%2.xlsx").arg(g_path).arg(currentPath);
	m_print->m_workbook->querySubObject("SaveCopyAs(QString)", QDir::toNativeSeparators(filepath));
	
//	m_print->m_worksheet->querySubObject("PrintPreview");

	//m_print->printview(filepath);
	emit processEnd(QStringLiteral("结束"));
//	m_print->m_excel->querySubObject("Quit()");
	

}
